require "grpc"
require "hello_world/greeter_server"

module HelloWorld
  GRPC::RpcServer.new.tap{|server|
    server.add_http2_port "0.0.0.0:3000", :this_port_is_insecure
    server.handle GreeterServer
    server.run_till_terminated_or_interrupted [ "SIGTERM" ]
  }
end
